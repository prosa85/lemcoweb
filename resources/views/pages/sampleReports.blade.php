@extends('layouts.base')

@section('title', 'Tampa home inspections in the Tampa Bay area.')
@section('description', 'Our Tampa Home Inspections include the following: 4 Point Inspection, Roof Condition &amp; Certification, Insurance &amp; Real Estate Inspection, Wind Mitigation')
@section('content')
<div id="main">
    <div class="content"><h1>Sample Inspection Reports</h1>
        <article id="post-11" class="post-11 page type-page status-publish hentry"><p>Our <strong>professionally
                    written inspection reports</strong> take time to prepare. After we verbally go over everything
                with you on the job site, we deliver your final type written report to you via fax or e-mail within
                24 hours after our home inspection is complete. <strong>Our reports are complete</strong> with
                photos, recommendations and easy to understand explanations!</p>
            <h3>Please view our sample home inspections below.</h3>
            <p><a target="_blank" href="inspections-examples/Sample-4Pt1.pdf">Sample 4 Point
                    Inspection</a></p>
            <p><a target="_blank" href="inspections-examples/Sample-Plumbing-Only2.pdf"
                  rel="noopener noreferrer">Sample Plumbing Inspection Only</a></p>
            <p><a target="_blank" href="inspections-examples/Sample-RC1.pdf">Sample RC</a></p>
            <p><a target="_blank" href="inspections-examples/Sample-WM1.pdf">Sample Wind
                    Mitigation</a></p>
            <p><a target="_blank" href="inspections-examples/Sample-WM1.pdf">Sample Uniform Mitigation Verification Inspection Form</a></p>
            <p>
                <script>//  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){ (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m) })(window,document,'script','//www.google-analytics.com/analytics.js','ga'); ga('create', 'UA-47396520-2', 'auto'); ga('send', 'pageview'); // ]]&gt;</script>
            </p>
            <div class="comments"></div>
            <br class="clear"></article>
    </div>
    @include('shared/sideform')
</div>
@endsection
