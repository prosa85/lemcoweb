@extends('layouts.base')

@section('title', 'Tampa home inspections in the Tampa Bay area.')
@section('description', 'Our Tampa Home Inspections include the following: 4 Point Inspection, Roof Condition &amp; Certification, Insurance &amp; Real Estate Inspection, Wind Mitigation')
@section('content')
    <div id="main">
        <div class="content"><h1>Insurance Inspections</h1>
            <article id="post-15" class="post-15 page type-page status-publish hentry">
                <p>
                    <script>//
                        (function (i, s, o, g, r, a, m) {
                            i['GoogleAnalyticsObject'] = r;
                            i[r] = i[r] || function () {<
                                br / >
                                (i[r].q = i[r].q || []).push(arguments)
                            }, i[r].l = 1 * new Date();
                            a = s.createElement(o),
                        <
                            br / >
                            m = s.getElementsByTagName(o)[0];
                            a.async = 1;
                            a.src = g;
                            m.parentNode.insertBefore(a, m) < br / >
                        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
                        <
                        /p>


                        < p > ga('create', 'UA-47396520-2', 'auto');
                        <
                        br / >
                        ga('send', 'pageview');
                        <
                        /p>


                        < p >// ]]&gt;</script>
                </p>
                <p style="text-align: left;">We provide both commercial real estate and home insurance inspections. See
                    below for a breakdown of the following inspection services.</p>
                <h2 style="text-align: left;">Common Insurance Inspections:</h2>
                <ul>
                    <li><strong>Roof Condition Certification</strong></li>
                    <li><strong>Mobile Home Tie-Down</strong></li>
                    <li><strong>Wind Loss Verification</strong></li>
                    <li><strong>4 Point</strong></li>
                    <li><a title="Real Estate Inspections"
                           href="http://www.lemcoinspections.com/real-estate-inspections/"><strong>Real Estate
                                Inspection</strong></a></li>
                </ul>
                <h3>The Roof Condition Certification Inspection</h3>
                <p><img class="alignleft wp-image-108 size-medium"
                        src="/img/insurance-inspection-250x195.jpg"
                        alt="insurance inspections" width="250" height="195"/>This real estate inspection is also
                    referred to as the standard Citizens Roof Condition Certification. This report is used to establish
                    the type of roofing material used, the approximate age of the roof, and its remaining potential
                    service life. Insurance companies use these criteria to value the deterioration and determine if any
                    repairs are necessary. Roofing repairs not dealt with immediately can raise safety concerns once the
                    structure is affected, and can also quickly lead to other costly repairs. LEMCO Inspections will
                    help you meet all of your roofing needs for insurance, loan or maintenance purposes.</p>
                <h3>Mobile Home Tie-Down Inspection</h3>
                <p>Tie-downs are systems of straps and anchors designed to stabilize mobile homes, also known as
                    manufactured homes. Failure to properly install and maintain the tie-downs on a mobile home reduces
                    its ability to resist sliding and overturning. If you want to get a home loan, we can evaluate your
                    mobile home’s tie-downs and anchoring according to Florida’s specifications so that you can satisfy
                    the tie-down inspection requirements of the loan. Mobile home tie-downs must be inspected to ensure
                    that they meet the weight limits required by Florida law. A mobile home tie-down inspection is also
                    required if you want to relocate a manufactured home. Used mobile homes should be checked regularly
                    for deterioration, especially before hurricane season. <a
                        href="http://www.nachi.org/manufactured-home-tie-downs.htm" target="_blank"
                        rel="noopener noreferrer">Click here</a> to learn more about mobile home tie-downs.</p>
                <h3>The Wind Loss Mitigation Verification Inspection</h3>
                <p>Florida Statute 626.0629 requires insurance companies to offer Florida homeowners “discounts,
                    credits, or other rate differentials” for construction techniques that reduce damage and loss in
                    windstorms. The Wind Loss Mitigation Verification Inspection will determine if you are qualified for
                    a discount off your homeowner’s insurance policy. The home inspector will evaluate all of the
                    typical construction features that reduce wind damage and loss, including roof-deck attachment,
                    secondary water resistance, roof shape, bracing of gable ends, protection of openings, roof-to-wall
                    connection, roof covering, doors and windows.</p>
                <h3>The 4 Point Inspection</h3>
                <p>This property inspection analyzes the following four systems: roofing, electrical, plumbing, and HVAC
                    (Heat, Ventilation &amp; Air Conditioning). This type of inspection is also sometimes referred to as
                    the Citizens 4 Point Inspection or the Tower Hill 4 Point Inspection. The criteria for this real
                    estate analysis, used to judge the condition of the four main systems, are the same as the criteria
                    used by most insurance companies. The inspector will evaluate the current operating conditions,
                    scheduled maintenance requirements, and expected service life of each of the four points.</p>
                <div class="comments"></div>
                <br class="clear"></article>
        </div>
        @include('shared/sideform')
    </div>
@endsection
