@extends('layouts.base')

@section('title', 'Contact us')
@section('description', 'Contact Us for all your real estate inspection needs.')
@section('content')
<div id="main">
    <div class="content"><h1>Contact Us for all your Insurance and Real Estate inspection needs.</h1>
        <article id="post-7" class="post-7 page type-page status-publish hentry"><p>LEMCO Inspections is here to
                serve you for all of your insurance and real estate inspection needs throughout the Tampa Bay area.
                We serve the following counties:</p>
            <ul>
                <li>Hillsborough</li>
                <li>Pinellas</li>
                <li>Manatee</li>
                <li>Polk</li>
                <li>Pasco</li>
                <li>Hernando</li>
                <li>Citrus</li>
            </ul>
            <p>Visit our <a href="https://www.facebook.com/tampahomeinspections" target="_blank" rel="noopener noreferrer">Facebook</a> page for information on what to look for during a real estate inspection.</p>
            <p>2117 W. Sewaha St<br/> Tampa, FL. 33612‏<br/> Phone: (813) 932-8515<br/> Fax: (813) 932-0602<br/>
                Email: <a href="mailto:Info@lemcoinspections.com">Info@lemcoinspections.com</a></p>
            <div id="vfb-form-1" class="visual-form-builder-container">
                @include('shared/contact-us-form')
            </div>
            <br class="clear"></article>
    </div>

</div>
@endsection
