@extends('layouts.base')

@section('title', 'Tampa home inspections in the Tampa Bay area.')
@section('description', 'Our Tampa Home Inspections include the following: 4 Point Inspection, Roof Condition &amp; Certification, Insurance &amp; Real Estate Inspection, Wind Mitigation')
@section('content')
    <div id="main">
        <div class="content"><h1>Real Estate Inspections</h1>
            <article id="post-13" class="post-13 page type-page status-publish hentry"><p style="text-align: left;">Why
                    are Real Estate Inspections so important? Real estate is the largest financial investment most
                    people make in their entire lifetimes. Real Estate inspections or commercial property inspections
                    will help lower your investment risk, and can also identify any potential safety concerns. Our real
                    estate inspectors are committed to providing you with valuable information about your new home or
                    commercial real estate purchase. They will give you the information you need to make an informed
                    decision about your important investment.</p>
                <p>Have your newly purchased property professionally inspected by LEMCO Inspections as early as possible
                    in the buying process to save money on hidden maintenance or insurance costs which you may be able
                    to have the seller or builder correct. Our real estate inspectors are able to professionally examine
                    every structural and mechanical component of your home or commercial property.</p>
                <p><img class="alignright wp-image-136 size-medium"
                        src="http://www.lemcoinspections.com/wp-content/uploads/2014/10/IMG_34571-250x187.jpg"
                        alt="Real Estate Inspections" width="250" height="187"/></p>
                <p>Have a real estate inspector examine your property and help you schedule any maintenance
                    requirements. Many costly repairs can be avoided by identifying damage or deterioration early,
                    before the problem worsens. Many older properties should be inspected regularly for general
                    maintenance repairs. Mobile/manufactured homes should have a tie-down inspection, especially before
                    hurricane season. Call LEMCO Inspections to make sure that both your property and your investment
                    are safe. We pride ourselves on providing prompt, professional, and courteous customer service.</p>
                <h3>Real Estate Inspections Reveals the Anatomy of a House</h3>
                <p>When you shop for a home, whether you are looking for a fixer-upper, new construction, or one in
                    move-in condition, there are generally essential components that make up any structure.</p>
                <h3>Electrical System</h3>
                <p>An electrical system is part of the mechanical components of the structure. A home inspector is not
                    required to report on Code violations, but he or she is required to conduct a thorough visual
                    examination of the visible aspects of a home’s electrical system. This includes outlets, fixtures
                    and circuit wiring.</p>
                <h3>Plumbing Inspections</h3>
                <p>A home inspector can only be responsible for the components that are visible. Much of your home’s
                    plumbing system is hidden inside walls. But what is visible will offer clues to your home’s plumbing
                    health. Galvanized pipe is often indicative of outdated material. Its corrosive qualities will cause
                    it to fail before other materials. Copper piping is preferable, but there are many other rigid and
                    semi-rigid synthetic materials that are long-lasting and affordable.</p>
                <h3>Structural Inspections</h3>
                <p>A structural inspection takes full inventory of those physical structures that literally hold a home
                    together. When they fall into disrepair, whether to ravages of time, natural disasters, or faulty
                    workmanship, they often cost big bucks to repair.</p>
                <br class="clear"></article>
        </div>
        @include('shared/sideform')
    </div>
@endsection
