@extends('layouts.base')

@section('title', 'Our agents')
@section('description', 'We provide the following inspections 4 Point Inspection, Roof Condition, Roof Certification, Insurance Inspection, Real Estate Inspection, General Home Inspection, Wind Mitigation, Citizens 4 Point, Tower Hill 4 Point, Universal 4 Point, Universal Wind Mitigation New purchase inspection, maintenance inspection, Warranty Inspection')
@section('content')
    <div id="main">
    <div class="content"><h1>Agents</h1>
        <article id="post-9" class="post-9 page type-page status-publish hentry">
            <p>
                <script>(function (i, s, o, g, r, a, m) {
                        i['GoogleAnalyticsObject'] = r;
                        i[r] = i[r] || function () {
                            (i[r].q = i[r].q || []).push(arguments)
                        }, i[r].l = 1 * new Date();
                        a = s.createElement(o),
                            m = s.getElementsByTagName(o)[0];
                        a.async = 1;
                        a.src = g;
                        m.parentNode.insertBefore(a, m)
                    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
                    <
                    /p>
                    < p > ga('create', 'UA-47396520-2', 'auto');
                    ga('send', 'pageview');
                    <
                    /p>
                    < p ></script>
            </p>
            <p style="text-align: center;"><strong>Click Here for a Copy of our: <a
                        href="http://www.lemcoinspections.com/wp-content/uploads/2014/10/Fax-Request-for-Inspection.pdf">Fax
                        Request for Inspection</a></strong></p>
            <p>Agents &#8211; ordering an inspection is easy. You can fill out a fax request form, email us or call
                and we will take care of the rest. Once we have scheduled your client for the inspection, we will
                contact you.</p>
            <p>Fast Scheduling: Your inspection can be completed within 24 – 48 hours of scheduling with us.
                Scheduling for the same day can be accommodated.<img class="alignright size-medium wp-image-146"
                                                                     src="img/IMG_34641-250x187.jpg"
                                                                     alt="IMG_3464"/></p>
            <p>Quick Turnaround: All completed inspections with pictures are sent to you by the close of the next
                business day and we can accommodate same day service if needed.</p>
            <p>Note: All inspections come with required photos.</p>
            <p>If you would like a copy of our price list, please contact us via phone, fax or email.</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <div class="comments"></div>
            <br class="clear"></article>
    </div>
    @include('shared/sideform')
</div>
@endsection
