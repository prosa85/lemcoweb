@extends('layouts.base')

@section('title', 'Tampa home inspections in the Tampa Bay area.')
@section('description', 'Our Tampa Home Inspections include the following: 4 Point Inspection, Roof Condition &amp; Certification, Insurance &amp; Real Estate Inspection, Wind Mitigation')
@section('content')
    <div id="main">
    <div class="content"><h1>About Us</h1>
        <article id="post-19" class="post-19 page type-page status-publish hentry">
            <p>
                <script><br />
                    (function (i, s, o, g, r, a, m) {
                        i['GoogleAnalyticsObject'] = r;
                        i[r] = i[r] || function () {<
                            br / >
                            (i[r].q = i[r].q || []).push(arguments)
                        }, i[r].l = 1 * new Date();
                        a = s.createElement(o),
                    <
                        br / >
                        m = s.getElementsByTagName(o)[0];
                        a.async = 1;
                        a.src = g;
                        m.parentNode.insertBefore(a, m) < br / >
                    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
                    <
                    /p>
                    < p > ga('create', 'UA-47396520-2', 'auto');
                    <
                    br / >
                    ga('send', 'pageview');
                    <
                    /p>
                    < p ></script>
            </p>
            <p><strong>With LEMCO Inspections, you always receive first-rate, professional real estate and home
                    inspection services. Call today to make an appointment with our highly qualified, NACHI backed
                    inspectors to analyze your building for insurance, maintenance, warranty, or new purchase
                    requirements. LEMCO Inspections has been providing home inspections since 1973.</strong></p>
            <p><strong>Location</strong></p>
            <p>Main office: 2117 W. Sewaha St., Tampa, Florida, 33612</p>
            <p>LEMCO Inspections is pleased to offer residential and commercial inspection services in Central
                Florida and the Tampa Bay area. Now servicing Hillsborough, Hernando, Manatee, Pinellas, Polk,
                Pasco, and Citrus counties, we are open from 8:30 a.m. to 5 p.m. (Saturday appointments are also
                available).</p>
            <p><strong>Services</strong></p>
            <p>We offer a comprehensive range of residential and commercial inspection options, including the 4
                Point Inspection, Wind Loss Mitigation Verification, Roof Condition Certification, and Mobile Home
                Tie Down Inspection:</p>
            <p><strong>4 Point Inspection</strong><br/> <a
                    href="img/IMG_34631.jpg"><img
                        class="alignright wp-image-145 size-medium"
                        src="img/IMG_34631-250x187.jpg"
                        alt="electrical home inspection" width="250" height="187"/></a>Call LEMCO Inspections for a
                comprehensive <strong>4 Point Inspection, </strong>universally known as the <strong>Citizens 4 Point
                    Inspection </strong>or the <strong>Tower Hill 4 Point Inspection,</strong> to thoroughly analyze
                your residential or commercial maintenance requirements. Save money by preventing costly future
                repairs with a general inspection that determines the true condition of the structure and its major
                subsystems: roofing, electrical, plumbing, and HVAC (Heating, Ventilation, and Air Conditioning).
                Insurance companies are expecting the condition of the four points listed above to be working as
                intended within the manufacturer’s specifications. If your home is an older home, the insurance
                companies want to see that the systems have been updated. The criteria used to judge the components
                include current operating conditions, scheduled maintenance requirements, and expected service life.
            </p>
            <p><strong>Wind Loss Mitigation Verification Inspection</strong></p>
            <p>Are you getting the most out of your home owner’s insurance? Florida Statute 626.0629 requires
                insurance companies to offer Florida homeowners “discounts, credits, or other rate differentials”
                for construction techniques that reduce damage and loss in windstorms. See how much money you could
                save with our universal<strong> Wind Loss Mitigation Verification Inspection</strong>, which has the
                potential to get you a discount off of your home owner’s insurance policy.</p>
            <p>LEMCO Inspections also offers a <strong>Roof Condition Certification Inspection </strong>and a
                <strong>Mobile Home Tie Down Inspection, </strong>as well general inspection services for new
                residential or commercial real estate purchases.</p>
            <p><strong>Staff </strong></p>
            <p>Family owned and operated since 1973, LEMCO Inspections is a full service residential and commercial
                inspection company committed to providing customers with prompt, courteous, and professional service
                for over 40 years. Founded by a state certified Class A general contractor, with a highly qualified
                staff consisting of state certified and insured home inspectors including a female inspector, and a
                certified master electrician, LEMCO Inspections is an NACHI backed company providing top-quality
                service from highly qualified real estate inspectors. We strive to excel as the best inspection
                company in the Central Florida and Tampa Bay area.</p>
            <div class="comments"></div>
            <br class="clear"></article>
    </div>
    @include('shared/sideform')
</div>
@endsection
