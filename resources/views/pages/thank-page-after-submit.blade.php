@extends('layouts.base')

@section('title', 'Tampa home inspections in the Tampa Bay area.')
@section('description', 'Our Tampa Home Inspections include the following: 4 Point Inspection, Roof Condition &amp; Certification, Insurance &amp; Real Estate Inspection, Wind Mitigation')
@section('content')
<div class="text-box">
    <div>
        Thank you for submitting a request for an inspection, one of our reps with contact you soon.
    </div>
    
@foreach ($request->all() as $key => $value)
    <strong>{ $key }}: </strong>
    Value: {{ $value }} <br>
@endforeach
    
</div>
@endsection
