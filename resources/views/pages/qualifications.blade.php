@extends('layouts.base')

@section('title', 'Tampa home inspections in the Tampa Bay area.')
@section('description', 'Our Tampa Home Inspections include the following: 4 Point Inspection, Roof Condition &amp; Certification, Insurance &amp; Real Estate Inspection, Wind Mitigation')
@section('content')
    <div id="main">
    <div class="content"><h1>Qualifications</h1>
        <article id="post-17" class="post-17 page type-page status-publish hentry"><p style="text-align: center;">
                Established in 1973 LEMCO is a full service inspection company. Performing Wind Mitigation, 4 Point
                Inspections, Roof Certifications &amp; Real Estate Inspections. Our company prides itself on
                providing prompt, courteous and professional service.</p>
            <p>Our Staff Includes:</p>
            <p><strong>Leroy E. McIver: </strong><em>Owner Operator</em></p>
            <p><strong>Charlene McIver: </strong><em>Inspector</em></p>
            <p><strong>Office Manager:</strong> Shannon Ballinger</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>
                <script>//  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){ (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m) })(window,document,'script','//www.google-analytics.com/analytics.js','ga'); ga('create', 'UA-47396520-2', 'auto'); ga('send', 'pageview'); // ]]&gt;</script>
            </p>
            <div class="comments"></div>
            <br class="clear"></article>
    </div>
    @include('shared/sideform')
</div>
@endsection
