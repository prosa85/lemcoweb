@extends('layouts.base')

@section('title', 'Tampa home inspections in the Tampa Bay area.')
@section('description', 'Our Tampa Home Inspections include the following: 4 Point Inspection, Roof Condition &amp; Certification, Insurance &amp; Real Estate Inspection, Wind Mitigation')
@section('content')

<div class="text-box">

    <div class="description">
        <div class="video-area"><h1 style="text-align: center;">Tampa Home Inspections</h1>

        </div>
        <div class="lc"><p><strong><img class="alignright wp-image-86 size-medium"
                                        src="img/home-250x230.png"
                                        alt="Tampa Home Inspections" width="250" height="230"
                                        /></strong></p>
            <p>LEMCO Inspections is a full service residential and commercial inspection company committed to
                providing our customers with prompt, courteous, and professional service. Family owned and
                operated since 1973, we have been building and inspecting homes and commercial properties for
                over 40 years. LEMCO Inspections is an NACHI backed company providing the highest quality of
                inspections by certified inspectors. Our licensed and insured staff of experienced real estate
                professionals will provide you with a thorough property examination and unmatched customer
                service. At LEMCO , we strive daily to excel as the best inspection company in the Central
                Florida and Tampa Bay area. Please visit our <a
                    href="https://www.facebook.com/tampahomeinspections">Facebook page</a> for all the latest
                news regarding Tampa Home Inspections.</p>
            <p>Why are property inspections so important? Real estate investments are risky, and having a
                complete and comprehensive knowledge of your property is key to lowering your investment risk.
                Our inspectors are committed to providing you with valuable information about your new home or
                commercial real estate purchase. We offer a 4 point inspection of all the major components of
                the property (roofing, electrical, plumbing and HVAC) to expose unknown defects and advise you
                about future maintenance requirements. Our company will give you the information you need to
                make an informed decision about your property. Having your newly purchased property
                professionally inspected by LEMCO Inspections as early as possible in the buying process could
                save you money on hidden maintenance or insurance costs which you may be able to have the seller
                or builder correct. Older homes and commercial properties should be inspected for general
                maintenance repairs regularly.</p>
            <p>Real estate repairs can be costly, especially roofing repairs, and preventive maintenance can
                save you a lot of money in the long run. LEMCO Inspections offers a Roofing Condition
                Certification Inspection to determine your roofing maintenance needs. And did you know that you
                could save money on your home owner’s insurance policy by having an inspection? Call LEMCO
                Inspections today and ask about the Wind Loss Mitigation Verification Inspection to find out how
                you could be eligible for a discount on your insurance!</p></div>
    </div>
</div>
@endsection
