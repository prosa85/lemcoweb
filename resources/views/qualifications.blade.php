<!doctype html>
<html lang="en-US" class="no-js">
<head>
    <meta name="google-site-verification" content="lVZZ0soKpdhWo8hvS2HdNTGCBY73CJHxq6NkNEpWSLM"/>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <meta name="description" content="TAMPA HOME INSPECTIONS">
    <link type="text/css" media="all"
          href="http://www.lemcoinspections.com/wp-content/cache/autoptimize/css/autoptimize_5985d916e04282b0db1fe1e391db5a91.css"
          rel="stylesheet"/>
    <title>Performing Wind Mitigation, 4 Point Inspections : Lemco Inspections</title>
    <link href="//www.google-analytics.com" rel="dns-prefetch">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
    <meta name="description" content="Save money on your homeowners insurance with a Wind Mitigation inspections"/>
    <meta name="robots" content="max-snippet:-1, max-image-preview:large, max-video-preview:-1"/>
    <link rel="canonical" href="http://www.lemcoinspections.com/wind-mitigation/"/>
    <meta property="og:locale" content="en_US"/>
    <meta property="og:type" content="article"/>
    <meta property="og:title" content="Performing Wind Mitigation, 4 Point Inspections"/>
    <meta property="og:description"
          content="Save money on your homeowners insurance with a Wind Mitigation inspections"/>
    <meta property="og:url" content="http://www.lemcoinspections.com/wind-mitigation/"/>
    <meta property="og:site_name" content="Lemco Inspections"/>
    <script type='application/ld+json' class='yoast-schema-graph yoast-schema-graph--main'>{
            "@context": "https://schema.org",
            "@graph": [
                {
                    "@type": "Organization",
                    "@id": "http://www.lemcoinspections.com/#organization",
                    "name": "Lemco Inspections",
                    "url": "http://www.lemcoinspections.com/",
                    "sameAs": [],
                    "logo": {
                        "@type": "ImageObject",
                        "@id": "http://www.lemcoinspections.com/#logo",
                        "url": "http://www.lemcoinspections.com/wp-content/uploads/2017/05/lemco.png",
                        "width": 280,
                        "height": 112,
                        "caption": "Lemco Inspections"
                    },
                    "image": {
                        "@id": "http://www.lemcoinspections.com/#logo"
                    }
                },
                {
                    "@type": "WebSite",
                    "@id": "http://www.lemcoinspections.com/#website",
                    "url": "http://www.lemcoinspections.com/",
                    "name": "Lemco Inspections",
                    "publisher": {
                        "@id": "http://www.lemcoinspections.com/#organization"
                    },
                    "potentialAction": {
                        "@type": "SearchAction",
                        "target": "http://www.lemcoinspections.com/?s={search_term_string}",
                        "query-input": "required name=search_term_string"
                    }
                },
                {
                    "@type": "WebPage",
                    "@id": "http://www.lemcoinspections.com/wind-mitigation/#webpage",
                    "url": "http://www.lemcoinspections.com/wind-mitigation/",
                    "inLanguage": "en-US",
                    "name": "Performing Wind Mitigation, 4 Point Inspections",
                    "isPartOf": {
                        "@id": "http://www.lemcoinspections.com/#website"
                    },
                    "datePublished": "2014-10-11T12:16:52+00:00",
                    "dateModified": "2017-06-01T21:41:28+00:00",
                    "description": "Save money on your homeowners insurance with a Wind Mitigation inspections"
                }
            ]
        }</script>
    <link rel='dns-prefetch' href='//cdnjs.cloudflare.com'/>
    <link rel='dns-prefetch' href='//s.w.org'/>
    <script type="text/javascript" data-cfasync="false">var mi_version = '7.10.0';
        var mi_track_user = true;
        var mi_no_track_reason = '';

        var disableStr = 'ga-disable-UA-100382536-1';

        /* Function to detect opted out users */
        function __gaTrackerIsOptedOut() {
            return document.cookie.indexOf(disableStr + '=true') > -1;
        }

        /* Disable tracking if the opt-out cookie exists. */
        if (__gaTrackerIsOptedOut()) {
            window[disableStr] = true;
        }

        /* Opt-out function */
        function __gaTrackerOptout() {
            document.cookie = disableStr + '=true; expires=Thu, 31 Dec 2099 23:59:59 UTC; path=/';
            window[disableStr] = true;
        }

        if (mi_track_user) {
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', '__gaTracker');

            __gaTracker('create', 'UA-100382536-1', 'auto');
            __gaTracker('set', 'forceSSL', true);
            __gaTracker('require', 'displayfeatures');
            __gaTracker('send', 'pageview');
        } else {
            console.log("");
            (function () {
                /* https://developers.google.com/analytics/devguides/collection/analyticsjs/ */
                var noopfn = function () {
                    return null;
                };
                var noopnullfn = function () {
                    return null;
                };
                var Tracker = function () {
                    return null;
                };
                var p = Tracker.prototype;
                p.get = noopfn;
                p.set = noopfn;
                p.send = noopfn;
                var __gaTracker = function () {
                    var len = arguments.length;
                    if (len === 0) {
                        return;
                    }
                    var f = arguments[len - 1];
                    if (typeof f !== 'object' || f === null || typeof f.hitCallback !== 'function') {
                        console.log('Not running function __gaTracker(' + arguments[0] + " ....) because you are not being tracked. " + mi_no_track_reason);
                        return;
                    }
                    try {
                        f.hitCallback();
                    } catch (ex) {

                    }
                };
                __gaTracker.create = function () {
                    return new Tracker();
                };
                __gaTracker.getByName = noopnullfn;
                __gaTracker.getAll = function () {
                    return [];
                };
                __gaTracker.remove = noopfn;
                window['__gaTracker'] = __gaTracker;
            })();
        }</script>
    <script type="text/javascript">window._wpemojiSettings = {
            "baseUrl": "https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/",
            "ext": ".png",
            "svgUrl": "https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/",
            "svgExt": ".svg",
            "source": {"concatemoji": "http:\/\/www.lemcoinspections.com\/wp-includes\/js\/wp-emoji-release.min.js"}
        };
        !function (a, b, c) {
            function d(a, b) {
                var c = String.fromCharCode;
                l.clearRect(0, 0, k.width, k.height), l.fillText(c.apply(this, a), 0, 0);
                var d = k.toDataURL();
                l.clearRect(0, 0, k.width, k.height), l.fillText(c.apply(this, b), 0, 0);
                var e = k.toDataURL();
                return d === e
            }

            function e(a) {
                var b;
                if (!l || !l.fillText) return !1;
                switch (l.textBaseline = "top", l.font = "600 32px Arial", a) {
                    case"flag":
                        return !(b = d([55356, 56826, 55356, 56819], [55356, 56826, 8203, 55356, 56819])) && (b = d([55356, 57332, 56128, 56423, 56128, 56418, 56128, 56421, 56128, 56430, 56128, 56423, 56128, 56447], [55356, 57332, 8203, 56128, 56423, 8203, 56128, 56418, 8203, 56128, 56421, 8203, 56128, 56430, 8203, 56128, 56423, 8203, 56128, 56447]), !b);
                    case"emoji":
                        return b = d([55357, 56424, 55356, 57342, 8205, 55358, 56605, 8205, 55357, 56424, 55356, 57340], [55357, 56424, 55356, 57342, 8203, 55358, 56605, 8203, 55357, 56424, 55356, 57340]), !b
                }
                return !1
            }

            function f(a) {
                var c = b.createElement("script");
                c.src = a, c.defer = c.type = "text/javascript", b.getElementsByTagName("head")[0].appendChild(c)
            }

            var g, h, i, j, k = b.createElement("canvas"), l = k.getContext && k.getContext("2d");
            for (j = Array("flag", "emoji"), c.supports = {
                everything: !0,
                everythingExceptFlag: !0
            }, i = 0; i < j.length; i++) c.supports[j[i]] = e(j[i]), c.supports.everything = c.supports.everything && c.supports[j[i]], "flag" !== j[i] && (c.supports.everythingExceptFlag = c.supports.everythingExceptFlag && c.supports[j[i]]);
            c.supports.everythingExceptFlag = c.supports.everythingExceptFlag && !c.supports.flag, c.DOMReady = !1, c.readyCallback = function () {
                c.DOMReady = !0
            }, c.supports.everything || (h = function () {
                c.readyCallback()
            }, b.addEventListener ? (b.addEventListener("DOMContentLoaded", h, !1), a.addEventListener("load", h, !1)) : (a.attachEvent("onload", h), b.attachEvent("onreadystatechange", function () {
                "complete" === b.readyState && c.readyCallback()
            })), g = c.source || {}, g.concatemoji ? f(g.concatemoji) : g.wpemoji && g.twemoji && (f(g.twemoji), f(g.wpemoji)))
        }(window, document, window._wpemojiSettings);</script>
    <script type='text/javascript' src='http://www.lemcoinspections.com/wp-includes/js/jquery/jquery.js'></script>
    <script type='text/javascript'
            src='http://cdnjs.cloudflare.com/ajax/libs/conditionizr.js/4.0.0/conditionizr.js'></script>
    <script type='text/javascript'
            src='http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.7.1/modernizr.min.js'></script>
    <script type='text/javascript'>var monsterinsights_frontend = {
            "js_events_tracking": "true",
            "download_extensions": "doc,pdf,ppt,zip,xls,docx,pptx,xlsx",
            "inbound_paths": "[]",
            "home_url": "http:\/\/www.lemcoinspections.com",
            "hash_tracking": "false"
        };</script>
    <link rel='https://api.w.org/' href='http://www.lemcoinspections.com/wp-json/'/>
    <link rel="alternate" type="application/json+oembed"
          href="http://www.lemcoinspections.com/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fwww.lemcoinspections.com%2Fwind-mitigation%2F"/>
    <link rel="alternate" type="text/xml+oembed"
          href="http://www.lemcoinspections.com/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fwww.lemcoinspections.com%2Fwind-mitigation%2F&#038;format=xml"/>
    <!--[if IE]>
    <script type="text/javascript"
            src="http://www.lemcoinspections.com/wp-content/themes/lemcoinspections/js/ie.js"></script><![endif]-->
    <script>// conditionizr.com
        // configure environment tests
        conditionizr.config({
            assets: 'http://www.lemcoinspections.com/wp-content/themes/lemcoinspections',
            tests: {}
        });</script>
</head>
<body class="page-template-default page page-id-17 wind-mitigation">
<div id="wrapper">
    <header id="header">
        <nav id="nav"><a class="opener fa fa-navicon" href="#"></a>
            <ul>
                <li id="menu-item-42"
                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-42"><a
                        href="/">Home</a></li>
                <li id="menu-item-35" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-35"><a
                        href="aboutUs">About Us</a></li>
                <li id="menu-item-36"
                    class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-17 current_page_item menu-item-36">
                    <a href="qualifications" aria-current="page">Qualifications</a></li>
                <li id="menu-item-37" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-37"><a
                        href="insurranceInspections">Insurance Inspections</a></li>
                <li id="menu-item-38" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-38"><a
                        href="realEstateInspections">Real Estate Inspections</a></li>
                <li id="menu-item-39" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-39"><a
                        href="sampleReports">Sample Reports</a></li>
                <li id="menu-item-40" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-40"><a
                        href="agents">Agents</a></li>
                <li id="menu-item-41" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-41"><a
                        href="contactUs">Contact Us</a></li>
            </ul>
        </nav>
        <div class="logo-area">
            <div class="banner-img"></div>
            <strong class="logo"> <a href="http://www.lemcoinspections.com"> <img
                        src="http://www.lemcoinspections.com/wp-content/themes/lemcoinspections/images/logo.png"
                        width="280" height="112" alt="Lemco Inspections"> </a> </strong>
            <div class="contact-info">
                <div id="text-2" class="widget_text">
                    <div class="textwidget">2117 W. Sewaha St <br/> Tampa, FL. 33612‏ <br/> Phone: (813) 932-8515 <br/>
                        Fax: (813) 932-0602 <br/> Email: <a href="mailto:Info@lemcoinspections.com">Info@lemcoinspections.com</a>
                        <br/></div>
                </div>
                <div id="cnss_widget-2" class="widget_cnss_widget">
                    <ul id="" class="cnss-social-icon " style="text-align:;"></ul>
                </div>
            </div>
        </div>
    </header>
    <div id="main">
        <div class="content"><h1>Qualifications</h1>
            <article id="post-17" class="post-17 page type-page status-publish hentry"><p style="text-align: center;">
                    Established in 1973 LEMCO is a full service inspection company. Performing Wind Mitigation, 4 Point
                    Inspections, Roof Certifications &amp; Real Estate Inspections. Our company prides itself on
                    providing prompt, courteous and professional service.</p>
                <p>Our Staff Includes:</p>
                <p><strong>Leroy E. McIver: </strong><em>Owner Operator</em></p>
                <p><strong>Charlene McIver: </strong><em>Inspector</em></p>
                <p><strong>Office Manager:</strong> Shannon Ballinger</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p>
                    <script>//  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){ (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m) })(window,document,'script','//www.google-analytics.com/analytics.js','ga'); ga('create', 'UA-47396520-2', 'auto'); ga('send', 'pageview'); // ]]&gt;</script>
                </p>
                <div class="comments"></div>
                <br class="clear"></article>
        </div>
        <aside id="sidebar" role="complementary">
            <div class="sidebar-widget">
                <div id="vfb_widget-2" class="vfb_pro_widget_class">
                    <div id="vfb-form-1" class="visual-form-builder-container">
                        <form id="contact-page-1" class="visual-form-builder vfb-form-1  " method="post"
                              enctype="multipart/form-data" action=""><input type="hidden" name="form_id" value="1"/>
                            <fieldset
                                class="vfb-fieldset vfb-fieldset-1 fill-out-form-below-to-request-more-information    "
                                id="item-vfb-1">
                                <div class="vfb-legend"><h3>Fill out form below to request more information</h3></div>
                                <ul class="vfb-section vfb-section-1">
                                    <li class="vfb-item vfb-item-text   vfb-left-half  " id="item-vfb-5"><label
                                            for="vfb-5" class="vfb-desc">First Name <span class="vfb-required-asterisk">*</span></label><input
                                            type="text" name="vfb-5" id="vfb-5" value=""
                                            class="vfb-text  vfb-medium  required  "/></li>
                                    <li class="vfb-item vfb-item-text   vfb-right-half  " id="item-vfb-6"><label
                                            for="vfb-6" class="vfb-desc">Last Name <span
                                                class="vfb-required-asterisk">*</span></label><input type="text"
                                                                                                     name="vfb-6"
                                                                                                     id="vfb-6" value=""
                                                                                                     class="vfb-text  vfb-medium  required  "/>
                                    </li>
                                    <li class="vfb-item vfb-item-text   vfb-left-half  " id="item-vfb-18"><label
                                            for="vfb-18" class="vfb-desc">Street Address <span
                                                class="vfb-required-asterisk">*</span></label><input type="text"
                                                                                                     name="vfb-18"
                                                                                                     id="vfb-18"
                                                                                                     value=""
                                                                                                     class="vfb-text  vfb-medium  required  "/>
                                    </li>
                                    <li class="vfb-item vfb-item-text   vfb-right-half  " id="item-vfb-19"><label
                                            for="vfb-19" class="vfb-desc">Apt, Suite, Bldg. (optional) </label><input
                                            type="text" name="vfb-19" id="vfb-19" value=""
                                            class="vfb-text  vfb-medium   "/></li>
                                    <li class="vfb-item vfb-item-text   vfb-left-half  " id="item-vfb-20"><label
                                            for="vfb-20" class="vfb-desc">City <span
                                                class="vfb-required-asterisk">*</span></label><input type="text"
                                                                                                     name="vfb-20"
                                                                                                     id="vfb-20"
                                                                                                     value=""
                                                                                                     class="vfb-text  vfb-medium  required  "/>
                                    </li>
                                    <li class="vfb-item vfb-item-text   vfb-right-half  " id="item-vfb-21"><label
                                            for="vfb-21" class="vfb-desc">State <span
                                                class="vfb-required-asterisk">*</span></label><input type="text"
                                                                                                     name="vfb-21"
                                                                                                     id="vfb-21"
                                                                                                     value=""
                                                                                                     class="vfb-text  vfb-medium  required  "/>
                                    </li>
                                    <li class="vfb-item vfb-item-number    " id="item-vfb-24"><label for="vfb-24"
                                                                                                     class="vfb-desc">Zip
                                            Code <span class="vfb-required-asterisk">*</span></label><input type="text"
                                                                                                            name="vfb-24"
                                                                                                            id="vfb-24"
                                                                                                            value=""
                                                                                                            class="vfb-text  vfb-medium  required  digits "/>
                                    </li>
                                    <li class="vfb-item vfb-item-phone   vfb-left-half  " id="item-vfb-8"><label
                                            for="vfb-8" class="vfb-desc">Phone <span
                                                class="vfb-required-asterisk">*</span></label><input type="tel"
                                                                                                     name="vfb-8"
                                                                                                     id="vfb-8" value=""
                                                                                                     class="vfb-text  vfb-medium  required  phone "/>
                                    </li>
                                    <li class="vfb-item vfb-item-email   vfb-right-half  " id="item-vfb-9"><label
                                            for="vfb-9" class="vfb-desc">Email <span
                                                class="vfb-required-asterisk">*</span></label><input type="email"
                                                                                                     name="vfb-9"
                                                                                                     id="vfb-9" value=""
                                                                                                     class="vfb-text  vfb-medium  required  email "/>
                                    </li>
                                    <li class="vfb-item vfb-item-text   vfb-left-half  " id="item-vfb-10"><label
                                            for="vfb-10" class="vfb-desc">Type of Inspection Requested <span
                                                class="vfb-required-asterisk">*</span></label><input type="text"
                                                                                                     name="vfb-10"
                                                                                                     id="vfb-10"
                                                                                                     value=""
                                                                                                     class="vfb-text  vfb-medium  required  "/>
                                    </li>
                                    <li class="vfb-item vfb-item-date   vfb-right-half  " id="item-vfb-12"><label
                                            for="vfb-12" class="vfb-desc">Preferred Date of Inspection <span
                                                class="vfb-required-asterisk">*</span></label><input type="text"
                                                                                                     name="vfb-12"
                                                                                                     id="vfb-12"
                                                                                                     value=""
                                                                                                     class="vfb-text vfb-date-picker  vfb-medium  required "
                                                                                                     data-dp-dateFormat="mm/dd/yy"/>
                                    </li>
                                    <li class="vfb-item vfb-item-text   vfb-left-half  " id="item-vfb-13"><label
                                            for="vfb-13" class="vfb-desc">Local Insurance Agency <span
                                                class="vfb-required-asterisk">*</span></label><input type="text"
                                                                                                     name="vfb-13"
                                                                                                     id="vfb-13"
                                                                                                     value=""
                                                                                                     class="vfb-text  vfb-medium  required  "/>
                                    </li>
                                    <li class="vfb-item vfb-item-text   vfb-right-half  " id="item-vfb-14"><label
                                            for="vfb-14" class="vfb-desc">Agent Name <span
                                                class="vfb-required-asterisk">*</span></label><input type="text"
                                                                                                     name="vfb-14"
                                                                                                     id="vfb-14"
                                                                                                     value=""
                                                                                                     class="vfb-text  vfb-medium  required  "/>
                                    </li>
                                    <li class="vfb-item vfb-item-text   vfb-left-half  " id="item-vfb-15"><label
                                            for="vfb-15" class="vfb-desc">Insurance Underwriter <span
                                                class="vfb-required-asterisk">*</span></label><input type="text"
                                                                                                     name="vfb-15"
                                                                                                     id="vfb-15"
                                                                                                     value=""
                                                                                                     class="vfb-text  vfb-medium  required  "/>
                                    </li>
                                    <li class="vfb-item vfb-item-text   vfb-right-half  " id="item-vfb-16"><label
                                            for="vfb-16" class="vfb-desc">Policy Number </label><input type="text"
                                                                                                       name="vfb-16"
                                                                                                       id="vfb-16"
                                                                                                       value=""
                                                                                                       class="vfb-text  vfb-medium   "/>
                                    </li>
                                    <li class="vfb-item vfb-item-textarea    " id="item-vfb-17"><label for="vfb-17"
                                                                                                       class="vfb-desc">Any
                                            additional information we should know? </label>
                                        <div><textarea name="vfb-17" id="vfb-17"
                                                       class="vfb-textarea  vfb-medium   "></textarea></div>
                                    </li>
                                    <li class="vfb-item vfb-item-email    " id="item-vfb-22"><label for="vfb-22"
                                                                                                    class="vfb-desc">Email </label><input
                                            type="email" name="vfb-22" id="vfb-22" value=""
                                            class="vfb-text  vfb-medium   email "/></li>
                                </ul>&nbsp;
                            </fieldset>
                            <fieldset class="vfb-fieldset vfb-fieldset-2 verification  " id="item-vfb-2">
                                <div class="vfb-legend"><h3>Verification</h3></div>
                                <ul class="vfb-section vfb-section-2">
                                    <li class="vfb-item vfb-item-secret"><label for="item-vfb-3" class="vfb-desc">Please
                                            enter any two digits</label><input type="hidden" name="_vfb-required-secret"
                                                                               value="0"/><input type="hidden"
                                                                                                 name="_vfb-secret"
                                                                                                 value="vfb-3"/><span
                                            class="vfb-span"><input type="text" name="vfb-3" id="item-vfb-3" value=""
                                                                    class="vfb-text  vfb-medium  {digits:true,maxlength:2,minlength:2} "/><label>Example: 12</label></span>
                                    <li style="display:none;"><label>This box is for spam protection - <strong>please
                                                leave it blank</strong></label>
                                        <div><input name="vfb-spam"/></div>
                                    </li>
                                    <li class="vfb-item vfb-item-submit" id="item-vfb-4"><input type="submit"
                                                                                                name="vfb-submit"
                                                                                                id="vfb-4"
                                                                                                value="Send Request"
                                                                                                class="vfb-submit "/>
                                    </li>
                                </ul> &nbsp;
                            </fieldset>
                            <input type="hidden" name="_wp_http_referer" value="/wind-mitigation/"/></form>
                    </div>
                </div>
            </div>
            <div class="sidebar-widget"></div>
        </aside>
    </div>
    <footer id="footer">
        <nav class="nav">
            <ul>
                <li id="menu-item-65"
                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-65"><a
                        href="http://www.lemcoinspections.com/">Home</a></li>
                <li id="menu-item-58" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-58"><a
                        href="http://www.lemcoinspections.com/about-us-home-inspections/">About Us</a></li>
                <li id="menu-item-59"
                    class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-17 current_page_item menu-item-59">
                    <a href="http://www.lemcoinspections.com/wind-mitigation/" aria-current="page">Qualifications</a>
                </li>
                <li id="menu-item-60" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-60"><a
                        href="http://www.lemcoinspections.com/insurance-inspections/">Insurance Inspections</a></li>
                <li id="menu-item-61" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-61"><a
                        href="http://www.lemcoinspections.com/real-estate-inspections/">Real Estate Inspections</a></li>
                <li id="menu-item-62" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-62"><a
                        href="http://www.lemcoinspections.com/sample-reports/">Sample Reports</a></li>
                <li id="menu-item-63" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-63"><a
                        href="http://www.lemcoinspections.com/agents-4-point-inspection/">Agents</a></li>
                <li id="menu-item-64" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-64"><a
                        href="http://www.lemcoinspections.com/contact-us-for-your-real-estate-inspections/">Contact
                        Us</a></li>
            </ul>
        </nav>
        <div class="info-area"><span class="copyright">&copy; 2015 <a href="#">Lemco Inspections</a>. All Rights Reserved. </span>
            <ul class="add-nav">
                <li id="menu-item-69" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-69"><a
                        href="#header">Back to top</a></li>
            </ul>
        </div>
    </footer>
</div>
<div id="rfwbs_slider" class="rfwbs_slider">
    <div class="rfwbs_container"><img class="rfwbs_bg"
                                      src="http://www.lemcoinspections.com/wp-content/uploads/2014/10/33.jpg"
                                      alt="rfwbs-slide"/><img class="rfwbs_bg"
                                                              src="http://www.lemcoinspections.com/wp-content/uploads/2014/10/22.jpg"
                                                              alt="rfwbs-slide"/><img class="rfwbs_bg"
                                                                                      src="http://www.lemcoinspections.com/wp-content/uploads/2014/10/11.jpg"
                                                                                      alt="rfwbs-slide"/></div>
    <nav class="rfwbs_navigation" style="display:none"><a href="#" class="rfwbs_next">Next</a> <a href="#"
                                                                                                  class="rfwbs_prev">Previous</a>
    </nav>
</div>
<script type="text/javascript">jQuery(function () {
        jQuery('#rfwbs_slider').superslides({
            animation: 'fade',
            animation_speed: 700,
            play: 8000,
            pagination: 0,
        });
    });

    jQuery(document).ready(function () {
        jQuery('#rfwbs_next_slide').click(function () {
            jQuery('.rfwbs_navigation .rfwbs_next').trigger('click');
        });
        jQuery('#rfwbs_prev_slide').click(function () {
            jQuery('.rfwbs_navigation .rfwbs_prev').trigger('click');
        });
        jQuery('#rfwbs_toggle').click(function () {
            var zindex = jQuery('.rfwbs_slider').css('z-index');
            if (zindex < 0) {
                jQuery('.rfwbs_slider').css('z-index', '999999999');
            } else {
                jQuery('.rfwbs_slider').css('z-index', '-1');
            }
        });
        jQuery('body').addClass('rfwbs-active');
    });</script>
<script
    type='text/javascript'>var VfbAjax = {"ajaxurl": "http:\/\/www.lemcoinspections.com\/wp-admin\/admin-ajax.php"};</script>
<script type="text/javascript" defer
        src="http://www.lemcoinspections.com/wp-content/cache/autoptimize/js/autoptimize_5d6a0458047efaad713a141df2a1a6c4.js"></script>
</body>
</html>
