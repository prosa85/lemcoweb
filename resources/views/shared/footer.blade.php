<footer id="footer">
    <nav class="nav">
        <ul>
            <li id="menu-item-65"
                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-5 current_page_item menu-item-65">
                <a href="/" aria-current="page">Home</a></li>
            <li id="menu-item-58" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-58"><a
                    href="/aboutUs">About Us</a></li>
            <li id="menu-item-59" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-59"><a
                    href="/qualifications">Qualifications</a></li>
            <li id="menu-item-60" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-60"><a
                    href="/insurranceInspections/">Insurance Inspections</a></li>
            <li id="menu-item-61" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-61"><a
                    href="/realEstateInspections/">Real Estate Inspections</a></li>
            <li id="menu-item-62" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-62"><a
                    href="/sampleReports/">Sample Reports</a></li>
            <li id="menu-item-63" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-63"><a
                    href="/agents/">Agents</a></li>
            <li id="menu-item-64" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-64"><a
                    href="/contactUs/">Contact
                    Us</a></li>
        </ul>
    </nav>
    <div class="info-area"><span class="copyright">&copy; 2015 <a href="#">Lemco Inspections</a>. All Rights Reserved. </span>
        <ul class="add-nav">
            <li id="menu-item-69" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-69"><a
                    href="#header">Back to top</a></li>
        </ul>
    </div>
</footer>
