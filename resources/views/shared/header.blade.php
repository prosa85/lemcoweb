<header id="header">
    <nav id="nav"><a class="opener fa fa-navicon" href="#"></a>
        <ul>
            <li id="menu-item-42"
                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-5 current_page_item menu-item-42">
                <a href="/" aria-current="page">Home</a></li>
            <li id="menu-item-35" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-35"><a
                    href="aboutUs">About Us</a></li>
            <li id="menu-item-36" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-36"><a
                    href="qualifications">Qualifications</a></li>
            <li id="menu-item-37" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-37"><a
                    href="insurranceInspections">Insurance Inspections</a></li>
            <li id="menu-item-38" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-38"><a
                    href="realEstateInspections">Real Estate Inspections</a></li>
            <li id="menu-item-39" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-39"><a
                    href="sampleReports">Sample Reports</a></li>
            <li id="menu-item-40" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-40"><a
                    href="agents">Agents</a></li>
            <li id="menu-item-41" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-41"><a
                    href="contactUs">Contact Us</a></li>
        </ul>
    </nav>
    <div class="logo-area">
        <div class="banner-img"></div>
        <strong class="logo"> <a href="/"> <img
                    src="img/logo.png"
                    width="280" height="112" alt="Lemco Inspections"> </a> </strong>
        <div class="contact-info">
            <div id="text-2" class="widget_text">
                <div class="textwidget">2117 W. Sewaha St <br/> Tampa, FL. 33612‏ <br/> Phone: (813) 932-8515 <br/>
                    Fax: (813) 932-0602 <br/> Email: <a href="mailto:Info@lemcoinspections.com">Info@lemcoinspections.com</a>
                    <br/></div>
            </div>
            <div id="cnss_widget-2" class="widget_cnss_widget">
                <ul id="" class="cnss-social-icon " style="text-align:;"></ul>
            </div>
        </div>
    </div>
</header>
