<aside id="sidebar" role="complementary">
    <div class="sidebar-widget">
        <div id="vfb_widget-2" class="vfb_pro_widget_class">
            <div id="vfb-form-1" class="visual-form-builder-container">
                <form id="contact-page-1" class="visual-form-builder vfb-form-1"
                      method="post"
                      action="/contact-us"
                      enctype="multipart/form-data" ><input type="hidden" name="form_id" value="1"/>
                    @csrf
                    <fieldset
                        class="vfb-fieldset vfb-fieldset-1 fill-out-form-below-to-request-more-information    "
                        id="item-vfb-1">
                        <div class="vfb-legend"><h3>Fill out form below to request more information</h3></div>
                        <ul class="vfb-section vfb-section-1">
                            <li class="vfb-item vfb-item-text   vfb-left-half  " id="item-vfb-5"><label
                                    for="vfb-5" class="vfb-desc">First Name <span class="vfb-required-asterisk">*</span></label>
                                <input
                                    type="text" name="vfb-5" id="vfb-5" value=""
                                    class="vfb-text  vfb-medium  required  " required /></li>
                            <li class="vfb-item vfb-item-text   vfb-right-half  " id="item-vfb-6"><label
                                    for="vfb-6" class="vfb-desc">Last Name <span
                                        class="vfb-required-asterisk">*</span></label>
                                <input type="text"
                                       name="vfb-6"
                                       id="vfb-6" value=""
                                       class="vfb-text  vfb-medium  required  " required />
                            </li>
                            <li class="vfb-item vfb-item-email   vfb-right-half  " id="item-vfb-9"><label
                                    for="vfb-9" class="vfb-desc">Email <span
                                        class="vfb-required-asterisk">*</span></label>
                                <input type="email"
                                       name="vfb-9"
                                       id="vfb-9" value=""
                                       class="vfb-text  vfb-medium  required  email " required/>
                            </li>
                            <li class="vfb-item vfb-item-phone   vfb-left-half  " id="item-vfb-8"><label
                                    for="vfb-8" class="vfb-desc">Phone <span
                                        class="vfb-required-asterisk">*</span></label>
                                <input type="tel"
                                       name="vfb-8"
                                       id="vfb-8" value=""
                                       class="vfb-text  vfb-medium  required  phone " required />
                            </li>
                            <li class="vfb-item vfb-item-text  " id="item-vfb-18"><label
                                    for="vfb-18" class="vfb-desc">Street Address <span
                                        class="vfb-required-asterisk">*</span></label>
                                <input type="text"
                                       name="vfb-18"
                                       id="vfb-18"
                                       value=""
                                       class="vfb-text  required  " required />
                            </li>
                            <!--                            <li class="vfb-item vfb-item-text   vfb-right-half  " id="item-vfb-19"><label-->
                            <!--                                    for="vfb-19" class="vfb-desc">Apt (optional) </label>
                            <input-->
                            <!--                                    type="text" name="vfb-19" id="vfb-19" value=""-->
                            <!--                                    class="vfb-text  vfb-medium   "/></li>-->
                            <li class="vfb-item vfb-item-text   vfb-left-half  " id="item-vfb-20"><label
                                    for="vfb-20" class="vfb-desc">City <span
                                        class="vfb-required-asterisk">*</span></label>
                                <input type="text"
                                       name="vfb-20"
                                       id="vfb-20"
                                       value=""
                                       class="vfb-text  vfb-medium  required  " required />
                            </li>
                            <li class="vfb-item vfb-item-text   vfb-right-half  " id="item-vfb-21"><label
                                    for="vfb-21" class="vfb-desc">State <span
                                        class="vfb-required-asterisk">*</span></label>
                                <input type="text"
                                       name="vfb-21"
                                       id="vfb-21"
                                       value=""
                                       class="vfb-text  vfb-medium  required  " required />
                            </li>
                            <li class="vfb-item vfb-item-number    " id="item-vfb-24"><label for="vfb-24" class="vfb-desc">Zip Code <span class="vfb-required-asterisk">*</span></label>
                                <input type="text"
                                       name="vfb-24"
                                       id="vfb-24"
                                       value=""
                                       class="vfb-text  vfb-medium  required  digits " required/>
                            </li>
                            <li class="vfb-item vfb-item-text   " id="item-vfb-10"><label
                                    for="vfb-10" class="vfb-desc">Type of Inspection Requested <span
                                        class="vfb-required-asterisk">*</span></label>
                                <input type="text"
                                       name="vfb-10"
                                       id="vfb-10"
                                       value=""
                                       class="vfb-text  required  " required />
                            </li>

                            <li class="vfb-item vfb-item-email    " id="item-vfb-22"><label for="vfb-22" class="vfb-desc">Email </label>
                                <input
                                    type="email" name="vfb-22" id="vfb-22" value=""
                                    class="vfb-text  email "/></li>
                            <li class="submit" id="item-vfb-22">
                                <button type="submit" id="vfb-23" class="submit-button">Submit</button></li>
                        </ul>&nbsp;
                    </fieldset>
                    <input type="hidden" name="_wp_http_referer" value="{{ Request::url() }}"/></form>
            </div>
        </div>
    </div>
    <div class="sidebar-widget"></div>
</aside>
