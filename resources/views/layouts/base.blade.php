<!doctype html>
<html lang="en-US" class="bg1">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <title>LEMCO - @yield('title')</title>
    <meta name="description" content="@yield('description')">
    <link href="{{ mix('/css/styles.css') }}" type="text/css" rel="stylesheet" />
    <!--    -->
    <meta name="google-site-verification" content="lVZZ0soKpdhWo8hvS2HdNTGCBY73CJHxq6NkNEpWSLM"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<!--    <link type="text/css" media="all"-->
<!--          href="http://www.lemcoinspections.com/wp-content/cache/autoptimize/css/autoptimize_5985d916e04282b0db1fe1e391db5a91.css"-->
<!--          rel="stylesheet"/>-->
    <link href="//www.google-analytics.com" rel="dns-prefetch">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>


    <meta name="robots" content="max-snippet:-1, max-image-preview:large, max-video-preview:-1"/>
    <link rel="canonical" href="http://www.lemcoinspections.com/"/>

<!--    <script type='text/javascript' src='http://www.lemcoinspections.com/wp-includes/js/jquery/jquery.js'></script>-->
    <script type='text/javascript'
            src='http://cdnjs.cloudflare.com/ajax/libs/conditionizr.js/4.0.0/conditionizr.js'></script>
    <script type='text/javascript'
            src='http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.7.1/modernizr.min.js'></script>

</head>
<body class="page-template-default page page-id-13 real-estate-inspections rfwbs-active">
<div id="wrapper" class="body-wrapper">
@include('shared.header')
<!--@section('sidebar')-->
<!--This is the master sidebar.-->
<!--@show-->

    <div id="main">
        @yield('content')
    </div>
    @include('shared.footer')
</div>

</body>
</html>
