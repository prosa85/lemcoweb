<!doctype html>
<html lang="en-US" class="no-js">
<head>
    <meta name="google-site-verification" content="lVZZ0soKpdhWo8hvS2HdNTGCBY73CJHxq6NkNEpWSLM"/>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <meta name="description" content="TAMPA HOME INSPECTIONS">
    <link type="text/css" media="all"
          href="http://www.lemcoinspections.com/wp-content/cache/autoptimize/css/autoptimize_5985d916e04282b0db1fe1e391db5a91.css"
          rel="stylesheet"/>
    <title>Tampa home inspections in the Tampa Bay area. : Lemco Inspections</title>
    <link href="//www.google-analytics.com" rel="dns-prefetch">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
    <meta name="description"
          content="Our Tampa Home Inspections include the following: 4 Point Inspection, Roof Condition &amp; Certification, Insurance &amp; Real Estate Inspection, Wind Mitigation"/>
    <meta name="robots" content="max-snippet:-1, max-image-preview:large, max-video-preview:-1"/>
    <link rel="canonical" href="http://www.lemcoinspections.com/"/>
    <meta property="og:locale" content="en_US"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="Tampa home inspections in the Tampa Bay area."/>
    <meta property="og:description"
          content="Our Tampa Home Inspections include the following: 4 Point Inspection, Roof Condition &amp; Certification, Insurance &amp; Real Estate Inspection, Wind Mitigation"/>
    <meta property="og:url" content="http://www.lemcoinspections.com/"/>
    <meta property="og:site_name" content="Lemco Inspections"/>
    <meta name="msvalidate.01" content="1BDF81BCCA9D34CFFE2B81C00547ACB2"/>
    <meta name="google-site-verification" content="ukEQ3Ueg17rRIaGGhaSW04hEmss4tL7WrgkQF9sqLH0"/>
    <meta name="yandex-verification" content="af3237c0459326b8"/>
    <script type='application/ld+json' class='yoast-schema-graph yoast-schema-graph--main'>
        {
            "@context": "https://schema.org",
            "@graph": [
                {
                    "@type": "Organization",
                    "@id": "http://www.lemcoinspections.com/#organization",
                    "name": "Lemco Inspections",
                    "url": "http://www.lemcoinspections.com/",
                    "sameAs": [],
                    "logo": {
                        "@type": "ImageObject",
                        "@id": "http://www.lemcoinspections.com/#logo",
                        "url": "http://www.lemcoinspections.com/wp-content/uploads/2017/05/lemco.png",
                        "width": 280,
                        "height": 112,
                        "caption": "Lemco Inspections"
                    },
                    "image": {
                        "@id": "http://www.lemcoinspections.com/#logo"
                    }
                },
                {
                    "@type": "WebSite",
                    "@id": "http://www.lemcoinspections.com/#website",
                    "url": "http://www.lemcoinspections.com/",
                    "name": "Lemco Inspections",
                    "publisher": {
                        "@id": "http://www.lemcoinspections.com/#organization"
                    },
                    "potentialAction": {
                        "@type": "SearchAction",
                        "target": "http://www.lemcoinspections.com/?s={search_term_string}",
                        "query-input": "required name=search_term_string"
                    }
                },
                {
                    "@type": "ImageObject",
                    "@id": "http://www.lemcoinspections.com/#primaryimage",
                    "url": "http://www.lemcoinspections.com/wp-content/uploads/2014/10/home.png",
                    "width": 637,
                    "height": 587
                },
                {
                    "@type": "WebPage",
                    "@id": "http://www.lemcoinspections.com/#webpage",
                    "url": "http://www.lemcoinspections.com/",
                    "inLanguage": "en-US",
                    "name": "Tampa home inspections in the Tampa Bay area.",
                    "isPartOf": {
                        "@id": "http://www.lemcoinspections.com/#website"
                    },
                    "about": {
                        "@id": "http://www.lemcoinspections.com/#organization"
                    },
                    "primaryImageOfPage": {
                        "@id": "http://www.lemcoinspections.com/#primaryimage"
                    },
                    "datePublished": "2014-10-11T12:13:07+00:00",
                    "dateModified": "2019-04-16T20:06:34+00:00",
                    "description": "Our Tampa Home Inspections include the following: 4 Point Inspection, Roof Condition & Certification, Insurance & Real Estate Inspection, Wind Mitigation"
                }
            ]
        }
    </script>
    <link rel='dns-prefetch' href='//cdnjs.cloudflare.com'/>
    <link rel='dns-prefetch' href='//s.w.org'/>
    <script type="text/javascript" data-cfasync="false">var mi_version = '7.10.0';
        var mi_track_user = true;
        var mi_no_track_reason = '';

        var disableStr = 'ga-disable-UA-100382536-1';

        /* Function to detect opted out users */
        function __gaTrackerIsOptedOut() {
            return document.cookie.indexOf(disableStr + '=true') > -1;
        }

        /* Disable tracking if the opt-out cookie exists. */
        if (__gaTrackerIsOptedOut()) {
            window[disableStr] = true;
        }

        /* Opt-out function */
        function __gaTrackerOptout() {
            document.cookie = disableStr + '=true; expires=Thu, 31 Dec 2099 23:59:59 UTC; path=/';
            window[disableStr] = true;
        }

        if (mi_track_user) {
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', '__gaTracker');

            __gaTracker('create', 'UA-100382536-1', 'auto');
            __gaTracker('set', 'forceSSL', true);
            __gaTracker('require', 'displayfeatures');
            __gaTracker('send', 'pageview');
        } else {
            console.log("");
            (function () {
                /* https://developers.google.com/analytics/devguides/collection/analyticsjs/ */
                var noopfn = function () {
                    return null;
                };
                var noopnullfn = function () {
                    return null;
                };
                var Tracker = function () {
                    return null;
                };
                var p = Tracker.prototype;
                p.get = noopfn;
                p.set = noopfn;
                p.send = noopfn;
                var __gaTracker = function () {
                    var len = arguments.length;
                    if (len === 0) {
                        return;
                    }
                    var f = arguments[len - 1];
                    if (typeof f !== 'object' || f === null || typeof f.hitCallback !== 'function') {
                        console.log('Not running function __gaTracker(' + arguments[0] + " ....) because you are not being tracked. " + mi_no_track_reason);
                        return;
                    }
                    try {
                        f.hitCallback();
                    } catch (ex) {

                    }
                };
                __gaTracker.create = function () {
                    return new Tracker();
                };
                __gaTracker.getByName = noopnullfn;
                __gaTracker.getAll = function () {
                    return [];
                };
                __gaTracker.remove = noopfn;
                window['__gaTracker'] = __gaTracker;
            })();
        }</script>
    <script type="text/javascript">window._wpemojiSettings = {
            "baseUrl": "https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/",
            "ext": ".png",
            "svgUrl": "https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/",
            "svgExt": ".svg",
            "source": {"concatemoji": "http:\/\/www.lemcoinspections.com\/wp-includes\/js\/wp-emoji-release.min.js"}
        };
        !function (a, b, c) {
            function d(a, b) {
                var c = String.fromCharCode;
                l.clearRect(0, 0, k.width, k.height), l.fillText(c.apply(this, a), 0, 0);
                var d = k.toDataURL();
                l.clearRect(0, 0, k.width, k.height), l.fillText(c.apply(this, b), 0, 0);
                var e = k.toDataURL();
                return d === e
            }

            function e(a) {
                var b;
                if (!l || !l.fillText) return !1;
                switch (l.textBaseline = "top", l.font = "600 32px Arial", a) {
                    case"flag":
                        return !(b = d([55356, 56826, 55356, 56819], [55356, 56826, 8203, 55356, 56819])) && (b = d([55356, 57332, 56128, 56423, 56128, 56418, 56128, 56421, 56128, 56430, 56128, 56423, 56128, 56447], [55356, 57332, 8203, 56128, 56423, 8203, 56128, 56418, 8203, 56128, 56421, 8203, 56128, 56430, 8203, 56128, 56423, 8203, 56128, 56447]), !b);
                    case"emoji":
                        return b = d([55357, 56424, 55356, 57342, 8205, 55358, 56605, 8205, 55357, 56424, 55356, 57340], [55357, 56424, 55356, 57342, 8203, 55358, 56605, 8203, 55357, 56424, 55356, 57340]), !b
                }
                return !1
            }

            function f(a) {
                var c = b.createElement("script");
                c.src = a, c.defer = c.type = "text/javascript", b.getElementsByTagName("head")[0].appendChild(c)
            }

            var g, h, i, j, k = b.createElement("canvas"), l = k.getContext && k.getContext("2d");
            for (j = Array("flag", "emoji"), c.supports = {
                everything: !0,
                everythingExceptFlag: !0
            }, i = 0; i < j.length; i++) c.supports[j[i]] = e(j[i]), c.supports.everything = c.supports.everything && c.supports[j[i]], "flag" !== j[i] && (c.supports.everythingExceptFlag = c.supports.everythingExceptFlag && c.supports[j[i]]);
            c.supports.everythingExceptFlag = c.supports.everythingExceptFlag && !c.supports.flag, c.DOMReady = !1, c.readyCallback = function () {
                c.DOMReady = !0
            }, c.supports.everything || (h = function () {
                c.readyCallback()
            }, b.addEventListener ? (b.addEventListener("DOMContentLoaded", h, !1), a.addEventListener("load", h, !1)) : (a.attachEvent("onload", h), b.attachEvent("onreadystatechange", function () {
                "complete" === b.readyState && c.readyCallback()
            })), g = c.source || {}, g.concatemoji ? f(g.concatemoji) : g.wpemoji && g.twemoji && (f(g.twemoji), f(g.wpemoji)))
        }(window, document, window._wpemojiSettings);</script>
    <script type='text/javascript' src='http://www.lemcoinspections.com/wp-includes/js/jquery/jquery.js'></script>
    <script type='text/javascript'
            src='http://cdnjs.cloudflare.com/ajax/libs/conditionizr.js/4.0.0/conditionizr.js'></script>
    <script type='text/javascript'
            src='http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.7.1/modernizr.min.js'></script>
    <script type='text/javascript'>var monsterinsights_frontend = {
            "js_events_tracking": "true",
            "download_extensions": "doc,pdf,ppt,zip,xls,docx,pptx,xlsx",
            "inbound_paths": "[]",
            "home_url": "http:\/\/www.lemcoinspections.com",
            "hash_tracking": "false"
        };</script>
    <link rel='https://api.w.org/' href='http://www.lemcoinspections.com/wp-json/'/>
    <link rel="alternate" type="application/json+oembed"
          href="http://www.lemcoinspections.com/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fwww.lemcoinspections.com%2F"/>
    <link rel="alternate" type="text/xml+oembed"
          href="http://www.lemcoinspections.com/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fwww.lemcoinspections.com%2F&#038;format=xml"/>
    <!--[if IE]>
    <script type="text/javascript"
            src="http://www.lemcoinspections.com/wp-content/themes/lemcoinspections/js/ie.js"></script><![endif]-->
    <script>// conditionizr.com
        // configure environment tests
        conditionizr.config({
            assets: 'http://www.lemcoinspections.com/wp-content/themes/lemcoinspections',
            tests: {}
        });</script>
</head>
<body class="home page-template page-template-homepage page-template-homepage-php page page-id-5">
<div id="wrapper">
    <header id="header">
        <nav id="nav"><a class="opener fa fa-navicon" href="#"></a>
            <ul>
                <li id="menu-item-42"
                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-5 current_page_item menu-item-42">
                    <a href="/" aria-current="page">Home</a></li>
                <li id="menu-item-35" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-35"><a
                        href="aboutUs">About Us</a></li>
                <li id="menu-item-36" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-36"><a
                        href="qualifications">Qualifications</a></li>
                <li id="menu-item-37" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-37"><a
                        href="insurranceInspections">Insurance Inspections</a></li>
                <li id="menu-item-38" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-38"><a
                        href="realEstateInspections">Real Estate Inspections</a></li>
                <li id="menu-item-39" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-39"><a
                        href="sampleReports">Sample Reports</a></li>
                <li id="menu-item-40" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-40"><a
                        href="agents">Agents</a></li>
                <li id="menu-item-41" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-41"><a
                        href="contactUs">Contact Us</a></li>
            </ul>
        </nav>
        <div class="logo-area">
            <div class="banner-img"></div>
            <strong class="logo"> <a href="http://www.lemcoinspections.com"> <img
                        src="http://www.lemcoinspections.com/wp-content/themes/lemcoinspections/images/logo.png"
                        width="280" height="112" alt="Lemco Inspections"> </a> </strong>
            <div class="contact-info">
                <div id="text-2" class="widget_text">
                    <div class="textwidget">2117 W. Sewaha St <br/> Tampa, FL. 33612‏ <br/> Phone: (813) 932-8515 <br/>
                        Fax: (813) 932-0602 <br/> Email: <a href="mailto:Info@lemcoinspections.com">Info@lemcoinspections.com</a>
                        <br/></div>
                </div>
                <div id="cnss_widget-2" class="widget_cnss_widget">
                    <ul id="" class="cnss-social-icon " style="text-align:;"></ul>
                </div>
            </div>
        </div>
    </header>
    <div id="main">
        <ul class="links">
            <li><a href=""> <img
                        src="http://www.lemcoinspections.com/wp-content/themes/lemcoinspections/images/img2.png"
                        alt="image description"> </a></li>
            <li><a href=""> <img
                        src="http://www.lemcoinspections.com/wp-content/themes/lemcoinspections/images/img3.png"
                        alt="image description"> </a></li>
            <li><a href=""> <img
                        src="http://www.lemcoinspections.com/wp-content/themes/lemcoinspections/images/img4.png"
                        alt="image description"> </a></li>
            <li><a href=""> <img
                        src="http://www.lemcoinspections.com/wp-content/themes/lemcoinspections/images/img5.png"
                        alt="image description"> </a></li>
        </ul>
        <div class="text-box">
            <div class="right-box">
                <div class="img-box"><img
                        src="http://www.lemcoinspections.com/wp-content/themes/lemcoinspections/images/img5.jpg"
                        alt="image description"></div>
                <div class="video"><img
                        src="http://www.lemcoinspections.com/wp-content/themes/lemcoinspections/images/placeholder2.jpg"
                        alt="image description"></div>
            </div>
            <div class="description">
                <p>
                    <script><br />
                        (function (i, s, o, g, r, a, m) {
                            i['GoogleAnalyticsObject'] = r;
                            i[r] = i[r] || function () {<
                                br / >
                                (i[r].q = i[r].q || []).push(arguments)
                            }, i[r].l = 1 * new Date();
                            a = s.createElement(o),
                        <
                            br / >
                            m = s.getElementsByTagName(o)[0];
                            a.async = 1;
                            a.src = g;
                            m.parentNode.insertBefore(a, m) < br / >
                        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
                        <
                        /p>
                        < p > ga('create', 'UA-47396520-2', 'auto');
                        <
                        br / >
                        ga('send', 'pageview');
                        <
                        /p>
                        < p ></script>
                </p>
                <div class="video-area"><h1 style="text-align: center;">Tampa Home Inspections</h1>
                    <p>
                        <iframe class="wistia_embed" style="display: block; margin: 0 auto;"
                                src="http://fast.wistia.net/embed/iframe/cibxh5fqe3" name="wistia_embed" width="480"
                                height="298" frameborder="0" scrolling="no"></iframe>
                    </p>
                </div>
                <div class="lc"><p><strong><img class="alignright wp-image-86 size-medium"
                                                src="http://www.lemcoinspections.com/wp-content/uploads/2014/10/home-250x230.png"
                                                alt="Tampa Home Inspections" width="250" height="230"
                                                srcset="http://www.lemcoinspections.com/wp-content/uploads/2014/10/home-250x230.png 250w, http://www.lemcoinspections.com/wp-content/uploads/2014/10/home-120x111.png 120w, http://www.lemcoinspections.com/wp-content/uploads/2014/10/home.png 637w"
                                                sizes="(max-width: 250px) 100vw, 250px"/></strong></p>
                    <p>LEMCO Inspections is a full service residential and commercial inspection company committed to
                        providing our customers with prompt, courteous, and professional service. Family owned and
                        operated since 1973, we have been building and inspecting homes and commercial properties for
                        over 40 years. LEMCO Inspections is an NACHI backed company providing the highest quality of
                        inspections by certified inspectors. Our licensed and insured staff of experienced real estate
                        professionals will provide you with a thorough property examination and unmatched customer
                        service. At LEMCO , we strive daily to excel as the best inspection company in the Central
                        Florida and Tampa Bay area. Please visit our <a
                            href="https://www.facebook.com/tampahomeinspections">Facebook page</a> for all the latest
                        news regarding Tampa Home Inspections.</p>
                    <p>Why are property inspections so important? Real estate investments are risky, and having a
                        complete and comprehensive knowledge of your property is key to lowering your investment risk.
                        Our inspectors are committed to providing you with valuable information about your new home or
                        commercial real estate purchase. We offer a 4 point inspection of all the major components of
                        the property (roofing, electrical, plumbing and HVAC) to expose unknown defects and advise you
                        about future maintenance requirements. Our company will give you the information you need to
                        make an informed decision about your property. Having your newly purchased property
                        professionally inspected by LEMCO Inspections as early as possible in the buying process could
                        save you money on hidden maintenance or insurance costs which you may be able to have the seller
                        or builder correct. Older homes and commercial properties should be inspected for general
                        maintenance repairs regularly.</p>
                    <p>Real estate repairs can be costly, especially roofing repairs, and preventive maintenance can
                        save you a lot of money in the long run. LEMCO Inspections offers a Roofing Condition
                        Certification Inspection to determine your roofing maintenance needs. And did you know that you
                        could save money on your home owner’s insurance policy by having an inspection? Call LEMCO
                        Inspections today and ask about the Wind Loss Mitigation Verification Inspection to find out how
                        you could be eligible for a discount on your insurance!</p></div>
            </div>
        </div>
        <section class="testimonials"><h3>See What Our Patients Say...</h3>
            <blockquote><q><p>“This spring I was a patient at your office. I was greeted with friendly and helpful
                        people throughout my visit. You were caring and concerned, taking time to speak with me. This
                        type of care is often rare but always appreciated. Please know that your kindness and compassion
                        mean very much to your patients. Thank you, Ian H.”</p> <a class="btn-testimonials" href="#">View
                        all</a> </q></blockquote>
        </section>
    </div>
    <footer id="footer">
        <nav class="nav">
            <ul>
                <li id="menu-item-65"
                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-5 current_page_item menu-item-65">
                    <a href="/" aria-current="page">Home</a></li>
                <li id="menu-item-58" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-58"><a
                        href="aboutUs">About Us</a></li>
                <li id="menu-item-59" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-59"><a
                        href="qualifications">Qualifications</a></li>
                <li id="menu-item-60" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-60"><a
                        href="http://www.lemcoinspections.com/insurance-inspections/">Insurance Inspections</a></li>
                <li id="menu-item-61" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-61"><a
                        href="http://www.lemcoinspections.com/real-estate-inspections/">Real Estate Inspections</a></li>
                <li id="menu-item-62" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-62"><a
                        href="http://www.lemcoinspections.com/sample-reports/">Sample Reports</a></li>
                <li id="menu-item-63" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-63"><a
                        href="http://www.lemcoinspections.com/agents-4-point-inspection/">Agents</a></li>
                <li id="menu-item-64" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-64"><a
                        href="http://www.lemcoinspections.com/contact-us-for-your-real-estate-inspections/">Contact
                        Us</a></li>
            </ul>
        </nav>
        <div class="info-area"><span class="copyright">&copy; 2015 <a href="#">Lemco Inspections</a>. All Rights Reserved. </span>
            <ul class="add-nav">
                <li id="menu-item-69" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-69"><a
                        href="#header">Back to top</a></li>
            </ul>
        </div>
    </footer>
</div>
<div id="rfwbs_slider" class="rfwbs_slider">
    <div class="rfwbs_container"><img class="rfwbs_bg"
                                      src="http://www.lemcoinspections.com/wp-content/uploads/2014/10/22.jpg"
                                      alt="rfwbs-slide"/><img class="rfwbs_bg"
                                                              src="http://www.lemcoinspections.com/wp-content/uploads/2014/10/11.jpg"
                                                              alt="rfwbs-slide"/><img class="rfwbs_bg"
                                                                                      src="http://www.lemcoinspections.com/wp-content/uploads/2014/10/33.jpg"
                                                                                      alt="rfwbs-slide"/></div>
    <nav class="rfwbs_navigation" style="display:none"><a href="#" class="rfwbs_next">Next</a> <a href="#"
                                                                                                  class="rfwbs_prev">Previous</a>
    </nav>
</div>
<script type="text/javascript">jQuery(function () {
        jQuery('#rfwbs_slider').superslides({
            animation: 'fade',
            animation_speed: 700,
            play: 8000,
            pagination: 0,
        });
    });

    jQuery(document).ready(function () {
        jQuery('#rfwbs_next_slide').click(function () {
            jQuery('.rfwbs_navigation .rfwbs_next').trigger('click');
        });
        jQuery('#rfwbs_prev_slide').click(function () {
            jQuery('.rfwbs_navigation .rfwbs_prev').trigger('click');
        });
        jQuery('#rfwbs_toggle').click(function () {
            var zindex = jQuery('.rfwbs_slider').css('z-index');
            if (zindex < 0) {
                jQuery('.rfwbs_slider').css('z-index', '999999999');
            } else {
                jQuery('.rfwbs_slider').css('z-index', '-1');
            }
        });
        jQuery('body').addClass('rfwbs-active');
    });
</script>
<script type="text/javascript" defer
        src="http://www.lemcoinspections.com/wp-content/cache/autoptimize/js/autoptimize_17647768a4ca776bd10e2c4453eed6e9.js">
</script>
</body>
</html>
