<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('home', function(){
    return view('pages.home');
});

Route::get('/', function () {
    return view('pages.home');
});
Route::get('aboutUs', function()
{
    return view('pages.about');
});
Route::get('qualifications', function()
{
    return view('pages.qualifications');
});
Route::get('insurranceInspections', function()
{
    return view('pages.insurranceInspections');
});
Route::get('realEstateInspections', function()
{
    return view('pages.realEstateInspections');
});
Route::get('sampleReports', function()
{
    return view('pages.sampleReports');
});
Route::get('agents', function()
{
    return view('pages.agents');
});

Route::get('contactUs', function()
{
    return view('pages.contactUs');
});

Route::post('contact-us', 'ContactusController@formsubmit');


