<?php

namespace App\Http\Controllers;

use App\Notifications\NewFormSubmittion;
use Illuminate\Support\Facades\Notification;
use Illuminate\Http\Request;

class ContactusController extends Controller
{

    public function sendNotification(Request $request){
    	$request->name = 'Pablo';
        $request->email = 'prosa85Reply@yahoo.com';
//        Notification::route('mail', ['prosa85@yahoo.com','info@lemcoinspections.com'])
        Notification::route('mail', 'prosa85@yahoo.com')
            ->notify(new NewFormSubmittion($request));
    }

    public function formsubmit(Request $request){
        
//        dd($request->all());
    	$this->sendNotification($request);
        return view('pages.thank-page-after-submit', compact('request'));

    }
}
