<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class NewFormSubmittion extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    private $email, $name;

    public function __construct($submitter)
    {
        $this->email = $submitter->email;
        $this->name = $submitter->name;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->from('prosa85@yahoo.com','Pablo Rosa')
                    ->replyTo($this->email)
                    ->greeting('Hi there from the web')
                    ->subject('This is a test email from the website')
                    ->line('A new form submission from the website.')
                    ->line('Name: '. $this->name)
                    ->line('Address: 9220 dalwood ct, tampa FL 33615')
                    ->line('Type of inspection: 4 point')
                    ->action('Notification Action', 'lemco.pablo-rosa.com');

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
